FROM mongo:4
WORKDIR /app
CMD ["./mongodump-to-s3.sh"]
RUN apt-get update && apt-get install -y awscli
COPY ./mongodump-to-s3.sh ./
RUN chmod a+x mongodump-to-s3.sh
