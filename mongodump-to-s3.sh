#!/usr/bin/env sh
FILE_NAME="mongodump_$(date '+%Y%m%d_%H%M%S')"
TAR_NAME="$FILE_NAME.tar.gz"
mongodump --uri="$MONGO_URI" --out=$FILE_NAME
tar -czvf "$TAR_NAME" "$FILE_NAME"
aws s3 cp "$TAR_NAME" "s3://$S3_PATH/$TAR_NAME"